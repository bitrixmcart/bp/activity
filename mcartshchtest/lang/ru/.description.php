<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$MESS['MCART_TEST_DESCR_NAME'] = 'ТЕСТ актвити [mcart]';
$MESS['MCART_TEST_DESCR_DESCR'] = 'Подсчет количества задач, находящихся в работе у пользователя [mcart]';

$MESS['MCART_TEST_TASK_CNT'] = 'Количество задач';
