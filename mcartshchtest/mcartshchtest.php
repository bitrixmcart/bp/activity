<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Bizproc\FieldType;

class CBPMcartShchTest extends CBPActivity
{
    public function __construct($name)
    {
        parent::__construct($name);
        $this->arProperties = array(
            "USER_ID" => "",
            "ResultCNT" => null,
        );

        $this->SetPropertiesTypes(
            array(
                'USER_ID' => array(
                    'Type' => FieldType::STRING
                  ),
        ));
    }

    public function Execute ()
    {

        if (!CModule::IncludeModule('tasks')) {
            return CBPActivityExecutionStatus::Closed;
        }

        if (!$this->USER_ID) {
            return CBPActivityExecutionStatus::Closed;
        }

        $arUser = CBPHelper::UsersStringToArray($this->USER_ID, $this->getDocumentType(), $arErrors);
        $this->ResultCNT = $this->getTaskCnt($arUser);

        return CBPActivityExecutionStatus::Closed;

    }

    public static function GetPropertiesDialog(
        $documentType,
        $activityName,
        $arWorkflowTemplate,
        $arWorkflowParameters,
        $arWorkflowVariables,
        $currentValues = null,
        $formName = "",
        $popupWindow = null,
        $siteId = ''
    ) {
        $dialog = new \Bitrix\Bizproc\Activity\PropertiesDialog(__FILE__, [
            'documentType' => $documentType,
            'activityName' => $activityName,
            'workflowTemplate' => $arWorkflowTemplate,
            'workflowParameters' => $arWorkflowParameters,
            'workflowVariables' => $arWorkflowVariables,
            'currentValues' => $currentValues,
            'formName' => $formName,
            'siteId' => $siteId,
        ]);

 
        if (!is_array($currentValues)) {
            $currentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
            if (isset($currentActivity["Properties"]['USER_ID'])) {
                $currentValues['user_id'] = $currentActivity["Properties"]['USER_ID'];
            }
        }

        $propertiesMap = array(
            "USER_ID" => array(
                'Name' => Loc::getMessage("MCART_TEST_USER_ID"),
				'Type' => 'string',
				'FieldName' => "user_id",
				'Required' => true
			),
        );
        $dialog->setMap($propertiesMap);

        return $dialog;
    }

    public static function GetPropertiesDialogValues(
        $documentType,
        $activityName,
        &$workflowTemplate,
        &$arWorkflowParameters,
        &$arWorkflowVariables,
        $currentValues,
        &$errors
    ) {
        $propertiesMap = array(
			"user_id" => "USER_ID",
		);

		$properties = array();
		foreach ($propertiesMap as $key => $value)
		{
			$properties[$value] = $currentValues[$key];
		}


        $errors = self::ValidateProperties(
            $properties,
            new CBPWorkflowTemplateUser(CBPWorkflowTemplateUser::CurrentUser)
        );
        if (count($errors) > 0) {
            return false;
        }

        $currentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($workflowTemplate, $activityName);
        $currentActivity["Properties"] = $properties;


        return true;
    }

    public static function ValidateProperties($properties = [], CBPWorkflowTemplateUser $user = null)
    {
        $errors = [];

        return array_merge($errors, parent::ValidateProperties($properties, $user));
    }

    private function getTaskCnt($userid) {
        
        if(! \Bitrix\Main\Loader::includeModule("tasks")){ 
            return 0;
		}

        $ids = array();
        if (is_array($userid)) {
            foreach ($userid as $value) {
                $v = str_replace("user_", "", $value);
                if ($v && (! in_array($v, $ids))) {
                    $ids[] = $v;
                }
            }
        } else {
            $ids[] = $userid;
        }

        $arStatus = array();
        $arStatus[] = \CTasks::STATE_PENDING;
        $arStatus[] = \CTasks::STATE_IN_PROGRESS;

        $dbQuery = new \Bitrix\Main\Entity\Query(\Bitrix\Tasks\Internals\TaskTable::getEntity());
        $dbQuery->setSelect(array(
            "ID",
        ));
        $dbQuery->whereIn('RESPONSIBLE_ID', $ids);
        $dbQuery->whereIn('STATUS', $arStatus);
        $queryCollection = $dbQuery->exec();

        $res = $dbQuery->fetchAll();

        return count($res);
    }

}