<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<?php
foreach ($dialog->getMap() as $fieldId => $field): ?>
	<tr>
		<td align="right" width="40%"><?= htmlspecialcharsbx($field['Name']) ?>:</td>
        <?php if ($field['FieldName'] === 'user_id'): ?>
            <td  width="60%">
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                    <tr>
                        <td>
                            <?= $dialog->renderFieldControl(
                                $field,
                                null,
                                false,
                                \Bitrix\Bizproc\FieldType::RENDER_MODE_DESIGNER
                            ) ?>
                        </td>
                        <td style="padding-left:4px">
                            <input type="button" value="..." onclick="BPAShowSelector('id_user_id', 'string');">
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        <?php else:?>
            <td  width="60%">
                <?= $dialog->renderFieldControl(
                    $field,
                    null,
                    false,
                    \Bitrix\Bizproc\FieldType::RENDER_MODE_DESIGNER
                ) ?>
            </td>
        <?php endif;?>
	</tr>
<?php endforeach; ?>

