<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

$arActivityDescription = [
    'NAME' => Loc::getMessage('MCART_TEST_DESCR_NAME'),
    'DESCRIPTION' => Loc::getMessage('MCART_TEST_DESCR_DESCR'),
    'TYPE' => ['activity'],
    'CLASS' => 'McartShchTest',
    'JSCLASS' => 'BizProcActivity',
    'CATEGORY' => [
        'ID' => 'document',
    ],
    'RETURN' => [
        'ResultCNT' => [
            'NAME' => Loc::getMessage('MCART_TEST_TASK_CNT'),
            'TYPE' => 'int',
        ],
    ]
];
